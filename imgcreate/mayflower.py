#
# mayflower.py : helper methods for creating Live CD images with mkinitrd < 6.0.31
#
# Copyright 2008, Red Hat  Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import os
import os.path
import shutil
import subprocess
from imgcreate.errors import *


MAYFLOWER_PATH = "/usr/bin/mayflower"

#
# Try to use mayflower if running from git tree
#
def mayflower_path():
    if not globals().has_key("__file__"):
        return MAYFLOWER_PATH

    pydir = os.path.abspath(os.path.dirname(__file__))
    if pydir.startswith("/usr/lib"):
        return MAYFLOWER_PATH

    git_mayflower = os.path.join(pydir, "../tools/mayflower")
    if not os.path.exists(git_mayflower):
        return MAYFLOWER_PATH

    return git_mayflower

def create_initramfs(instroot, chroot, kernels):
    mayflower = mayflower_path()
    if not os.path.isfile(mayflower):
        raise CreatorError("livecd-creator not correctly installed : "
                           "%s not found" % MAYFLOWER_PATH)

    shutil.copy(mayflower, instroot + "/sbin")

    for kernel in kernels:
        for version in kernels[kernel]:
            subprocess.call(["/sbin/mayflower", "-f",
                             "/boot/initrd-%s.img" % (version,),
                             version],
                            preexec_fn=chroot),

    os.unlink(instroot + "/sbin/mayflower")
    os.unlink(instroot + "/etc/mayflower.conf")
